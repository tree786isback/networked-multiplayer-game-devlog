# Networked Multiplayer Game -- Devlog

Archive showing attempts at using various libs to develop a networked game with vehicles and voxel-like building.

## Background
I decided to try and create two versions of a networked game - one in c++, and another in rust. Both projects use different libraries. But the idea was to have them be as close as possible, from both a visual and programming point-of-view. Both projects use ECS (Entity-Component-System) libs, on the server and client. Networking structure is also similar, as they have an almost fully authorative server model. Similar mechanics and features include:
  - First/third camera for character
  - Voxel-style building
  - Cars (and planes in the case of rust project)
  - Chatbox
  - Simple lobby system and pause menu's

### Notable differences between both projects:
  - Rust project has planes
  - Rust project has "sticky" particles
  - C++ has screen shader support. This includes:
    * Depth based fog
    * Depth based camera motion blur
    * Vignette
    * Speed based color distortion
    * 2-pass box blur for menu's (lobby/pause)

The rust project has been abandoned for an unknown period. These is due to several reasons, including dissapointment with the WGPU backend. Currently, the C++ project is under development (OpenGL 3.3).
## Screenshots

### (1) Using rust
* Abandoned (at least for now)

- Connect to a server:

![Connect to server](/Peek_2022-02-13_18-35.gif)

- Fly jets:

![Fly planes](/plane.gif)

- Modular building system

![Voxel-like building system](/blocks.gif)

#### (2) Using C++

- Connect to a server:

![Connect to server](/cpp_connect.gif)

- Modular building system

![Voxel-like building system](/blockcpp.gif)
